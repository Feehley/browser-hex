let fileBytes = [];
let darkModeEnabled = false;
const highlightedBytes = [];


function resetPage() {
    location.reload();
}


function toggleDarkMode() {
    const body = document.body;
    darkModeEnabled = !darkModeEnabled;

    if (darkModeEnabled) {
        body.classList.add('dark-mode');
        adjustHighlightTextColor(true);
    } else {
        body.classList.remove('dark-mode');
        adjustHighlightTextColor(false);
    }
}


function adjustHighlightTextColor(isDarkMode) {
    const highlightedElements = document.querySelectorAll('.highlighted, .highlighted-space, .highlighted-text');

    highlightedElements.forEach(element => {
        const backgroundColor = getComputedStyle(element).backgroundColor;
        const textColor = isDarkMode ? getIdealTextColorDarkMode(backgroundColor) : getIdealTextColor(backgroundColor);
        element.style.color = textColor;
    });
}


function getIdealTextColor(bgColor) {
    const nThreshold = 105;
    const components = getRGBComponents(bgColor);
    const bgBrightness = components.R * 0.299 + components.G * 0.587 + components.B * 0.114;

    return bgBrightness > nThreshold ? '#000000' : '#ffffff';
}


function getIdealTextColorDarkMode(bgColor) {
    const nThreshold = 105;
    const components = getRGBComponents(bgColor);
    const bgBrightness = components.R * 0.299 + components.G * 0.587 + components.B * 0.114;

    return bgBrightness > nThreshold ? '#ffffff' : '#000000';
}


function getRGBComponents(color) {
    const r = parseInt(color.substring(1, 3), 16);
    const g = parseInt(color.substring(3, 5), 16);
    const b = parseInt(color.substring(5, 7), 16);

    return { R: r, G: g, B: b };
}


function uploadFile() {
    const fileInput = document.getElementById('fileInput');
    const file = fileInput.files[0];

    if (file) {
        const reader = new FileReader();
        reader.onload = function(e) {
            const arrayBuffer = e.target.result;
            const uint8Array = new Uint8Array(arrayBuffer);
            fileBytes = Array.from(uint8Array);

            displayBytesInHexView();
            displayInterpretedText();
        };
        reader.readAsArrayBuffer(file);
    }
}


function clearHighlights() {
    const highlightedElements = document.querySelectorAll('.highlighted, .highlighted-space, .highlighted-text');

    highlightedElements.forEach(element => {
        element.classList.remove('highlighted', 'highlighted-space', 'highlighted-text');
        element.removeAttribute('style');
    });
}


function displayBytesInHexView() {
    const hexView = document.getElementById('hexView');
    hexView.innerHTML = '';
    const bytesPerRow = 16;
    let hexBytes = '';

    const hexAddrView = document.getElementById('hexAddr');
    hexAddrView.innerHTML = '';
    let hexAddr = '';
    let line_addr = 0;

    for (let i = 0; i < fileBytes.length; i += bytesPerRow) {
        const chunk = fileBytes.slice(i, i + bytesPerRow);

        for (let j = 0; j < chunk.length; j++) {
            const byteHex = chunk[j].toString(16).padStart(2, '0');
            const index = i + j;
            const highlightInfo = getHighlightInfo(index);
            const isHighlighted = highlightInfo.isHighlighted;
            const highlightColor = highlightInfo.highlightColor;
            const definition = highlightInfo.definition;
            hexBytes += isHighlighted
                ? `<span class="highlighted" style="background-color: ${highlightColor}" data-definition="${definition}">${byteHex} </span>`
                : `${byteHex} `;
        }
        line_addr = i;
        if (line_addr >= 0){
            hexAddr += "\n";
            hexAddr += `<span class="addrs">${line_addr.toString(16).padStart(8,'0')}</span>`;
        }
        hexBytes += '\n';
    }
    line_addr += bytesPerRow;
    hexAddrView.innerHTML += hexAddr;
    hexView.innerHTML = hexBytes;
}


function displayInterpretedText() {
    const interpretedText = document.getElementById('interpretedText');
    interpretedText.innerHTML = '';
    const bytesPerRow = 16;
    let interpretedTextContent = '';

    const textAddrView = document.getElementById('interpretedAddr');
    textAddrView.innerHTML = '';
    let textAddr = '';
    let line_addr = 0;

    for (let i = 0; i < fileBytes.length; i += bytesPerRow) {
        const chunk = fileBytes.slice(i, i + bytesPerRow);

        for (let j = 0; j < chunk.length; j++) {
            const byte = chunk[j];
            const index = i + j;
            const highlightInfo = getHighlightInfo(index);
            const isHighlighted = highlightInfo.isHighlighted;
            const highlightColor = highlightInfo.highlightColor;
            const definition = highlightInfo.definition;
            const interpretedChar = (byte >= 32 && byte <= 126) ? String.fromCharCode(byte) : '.';
            interpretedTextContent += isHighlighted
                ? `<span class="highlighted-text" style="background-color: ${highlightColor}" data-definition="${definition}">${interpretedChar}</span>`
                : `${interpretedChar}`;
        }

        line_addr = i;
        if (line_addr >= 0){
            textAddr += "\n";
            textAddr += `<span class="addrs">${line_addr.toString(16).padStart(8,'0')}</span>`;
        }
        interpretedTextContent += '\n';
    }
    line_addr += bytesPerRow;
    textAddrView.innerHTML += textAddr;
    interpretedText.innerHTML = interpretedTextContent;
}


function hex_to_dec(num){
    if (num.includes("0x")){
        return parseInt(num, 16);
    } else {
        return parseInt(num);
    }
}


function highlightBytes() {
    const dataVariablesInput = document.getElementById('dataVariables');
    const dataVariables = dataVariablesInput.value;

    highlightedBytes.length = 0;

    const dataDefinitions = dataVariables.split('\n').map(line => line.trim()).filter(line => line.length > 0);

    let currentIndex = 0;
    for (const line of dataDefinitions) {
        const matches = line.match(/(\w+)\[(\d+|0x[0-9a-fA-F]+)\];$/);
        const offset_matches = line.match(/(\w+)\[(\d+|0x[0-9a-fA-F]+)]@(\d+|0x[0-9a-fA-F]+);/g);

        if (offset_matches){
            const variable = line.split('[')[0];
            const length = parseInt(line.split('[')[1].split(']')[0]);
            var startByte = hex_to_dec(line.split('@')[1]);
            var endByte = startByte + length;
            const highlightColor = getRandomHighlightColor();

            for (let i = startByte; i < endByte; i++) {
                highlightedBytes.push({ index: i, highlightColor, definition: `${variable}[${length}]` });
                if (i < endByte - 1) {
                    highlightedBytes.push(-1);
                }
            }

            currentIndex = endByte;
        } else if (matches) {
            const variable = matches[1];
            const length = hex_to_dec(matches[2]);
            const startByte = currentIndex;
            const endByte = currentIndex + length;
            const highlightColor = getRandomHighlightColor();

            for (let i = startByte; i < endByte; i++) {
                highlightedBytes.push({ index: i, highlightColor, definition: `${variable}[${length}]` });
                if (i < endByte - 1) {
                    highlightedBytes.push(-1);
                }
            }

            currentIndex = endByte;
        }
    }

    displayBytesInHexView();
    displayInterpretedText();
}


function getHighlightInfo(index) {
    const defaultHighlightColor = 'rgba(255, 235, 59, 0.7)';
    for (const highlightedByte of highlightedBytes) {
        if (highlightedByte.index === index) {
            return {
                isHighlighted: true,
                highlightColor: highlightedByte.highlightColor || defaultHighlightColor,
                definition: highlightedByte.definition || '',
            };
        }
    }
    return { isHighlighted: false, highlightColor: '', definition: '' };
}


function getRandomHighlightColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
