const toggleButton = document.getElementById('toggleButton');
const popupMenu = document.getElementById('popupMenu');

function toggleMenu(){
    if (popupMenu.style.display === 'none' || popupMenu.style.display === '') {
        popupMenu.style.display = 'block';
    } else {
        popupMenu.style.display = 'none';
    }
}


function toggleAddrs(){
    const hexAddrView = document.getElementById('hexAddr');
    const textAddrView = document.getElementById('interpretedAddr');

    if (hexAddrView.style.display === 'none' || hexAddrView.style.display === ''){
        hexAddrView.style.display = 'block';
    } else {
        hexAddrView.style.display = 'none';
    }

    if (textAddrView.style.display === 'none' || textAddrView.style.display === ''){
        textAddrView.style.display = 'block';
    } else {
        textAddrView.style.display = 'none';
    }

}

function removeLastSpace(str){
    if (str.charAt(str.length - 1) == " "){
        return str.slice(0, -1)
    }
    return str
}


function getParentDiv(element) {
    while (element && element.tagName !== 'DIV') {
        element = element.parentElement;
    }
    return element;
}


function copyArguments(selection){
    navigator.clipboard.writeText(selection)
    alert("Copied text");
}

function getCountBytes(selection){
    let parentDiv;
    if (selection && selection.anchorNode) {
        parentDiv = getParentDiv(selection.anchorNode.parentElement);
    }

    if (parentDiv) {
        const parentDivId = parentDiv.id;

        if (parentDivId === 'hexView') {
            var selectedText = selection.toString();
            selectedText = selectedText.replace(" ", '');
            return Math.round(selectedText.length / 3);
        } else if (parentDivId === 'interpretedText') {
            const interpretedText = removeLastSpace(selection.toString());
            return Math.round(interpretedText.length) -1;
        }
    }

    return '';
}


function getSelectionText(arg) {
    const selection = window.getSelection();
    if (arg == "count"){
        return getCountBytes(selection);
    } else if (arg == "copy"){
        copyArguments(selection);
    }

}

document.addEventListener('keydown', (e) => {
    if (e.target.nodeName != "TEXTAREA"){
        if (e.keyCode == 72 || e.keyCode == 27){ // "h" or ESC tool tips
            toggleMenu();
        } else if (e.keyCode === 82){ // "r" run query -- highlight bytes
            highlightBytes();
        } else if (e.keyCode === 84){ // "t" toggle dark mode
            toggleDarkMode();
        } else if (e.keyCode === 78) { // "n" count bytes
            document.getElementById("dataVariables").value +=
                "temp[" + getSelectionText("count") + "];\n";
        } else if (e.keyCode ===  88){ // "x" copy to clipboard
            getSelectionText("copy");
        } else if (e.keyCode == 67){ // "c" clear highlights
            clearHighlights();
        } else if (e.keyCode == 65){
            toggleAddrs();
        //} else {
        //    console.log(e.key, e.keyCode);
        }
    } else{
        if (e.keyCode == 186){
            e.preventDefault();
            var datavar = document.getElementById("dataVariables");
            datavar.value += ";\n";
        }
    }
});
/* 
 * Basic Example on start
 * Mobile mode
*/
